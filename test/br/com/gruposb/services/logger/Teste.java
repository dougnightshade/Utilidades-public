/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.gruposb.services.logger;

import br.com.gruposb.servicos.logger.LoggerService;

/**
 *
 * @author douglas.santos
 */
public class Teste {
    
    public static void main(String[] args) {
        
        LoggerService obLoggerService = new LoggerService(Teste.class);
        
        obLoggerService.info("Iniciando teste de log");
        obLoggerService.debug("Teste de DEBUG");
        obLoggerService.warning("Teste de WARNING");
        
        try {
            Exception e = new Exception();
            
            throw e;
        } catch (Exception e) {
            obLoggerService.error(e, "main");
        }
        
        
    }
    
}
