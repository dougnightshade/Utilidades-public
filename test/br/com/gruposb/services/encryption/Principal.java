/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.gruposb.services.encryption;

import br.com.gruposb.servicos.criptografia.EncryptionService;

/**
 *
 * @author douglas.santos
 */
public class Principal {

    static EncryptionService obEncryptionService = new EncryptionService();

    public static void main(String[] args) throws Exception {

        String textoPuro = "Douglas";
        String textoEncryptado;
        String textoDecriptado;
        byte[] byteEncryptado;

        System.out.println("Texto puro: ");
        System.out.println(textoPuro);

        byteEncryptado = obEncryptionService.encrypt(textoPuro);

        System.out.println("Texto encriptado");
        System.out.println(obEncryptionService.byteToString(byteEncryptado));

        textoDecriptado = obEncryptionService.decrypt(byteEncryptado);

        System.out.println("Texto decryptado");
        System.out.println(textoDecriptado);

    }

}
